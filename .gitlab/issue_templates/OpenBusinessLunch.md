Short abstract in german or english.

## speaker

A few words describing the speaker / references. A picture is also welcome.

## additional information

Is recording and publishing afterwards e.g. on YouTube allowed?
- [ ] Yes of course!
- [ ] No, please not!

---

# orga

The single-source of truth is the information present here within the issue in gitlab.

## team
- @MarkusTiede
- @NathalieSinz
- @RthollIBM

## mode
- [ ] virtual via BBB: https://bbb.ch-open.ch/b/cho-hwi-yre-grj
- [ ] on site 
- [ ] hybrid (on site as well as virtual)

## checklist
- [ ] find / pick / propose [date](https://gitlab.com/ch-open/obe/-/blob/master/README.md#potential-dates)
- [ ] publish information on https://www.ch-open.ch
- [ ] publish [meetup event here](https://www.meetup.com/de-DE/Open-Source-Forderer-in-der-Schweiz/)
- [ ] cross-reference event on other channels (LinkedIn, gi.de, ...)
- [ ] record session on BBB
- [ ] publish recording on [YouTube](https://www.youtube.com/c/CHOpen)
