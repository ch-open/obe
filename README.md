# OBE

## propose one on your own by simply [filing a new issue](https://gitlab.com/ch-open/obe/-/issues/new?issuable_template=OpenBusinessLunch)

CH [Open Business Events](https://www.ch-open.ch/ch-open-business-events/)

## Open Business Café - OBC

### 29.04.2021 OSS Kaffee in Bern : https://gitlab.com/ch-open/obe/-/issues/21
Falls möglich physisch vor Ort im Toi et Moi.

## [Open Business Lunch - OBL](.gitlab/issue_templates/OpenBusinessLunch.md)

## Open Business Apéro - OBA

# timeline

## potential dates

### OBL 25.03.2022 with @MarkusTiede : https://gitlab.com/ch-open/obe/-/issues/26
backup: 29.04.2022

### OBL 20.05.2022 with @MarkusTiede : `slot available`
backup: 24.06.2022

### OBL 26.08.2022 with @MarkusTiede : `slot available`
backup: 23.09.2022

### OBL 27.10.2022 with @MarkusTiede : `slot available`
backup: 25.11.2022

### OBL 16.12.2022 with @MarkusTiede : `slot available`

## recent

### `en` 30.09.2021 - CH Open Business Lunch > [Jakarta EE Virtual Tour: Jakarta EE 8 and Beyond](https://www.ch-open.ch/ch-open-business-events/ch-open-business-lunch/jakarta-ee-virtual-tour-jakarta-ee-8-and-beyond/)

[![YouTube Video Link](https://img.youtube.com/vi/CwdI-6pmFm4/0.jpg)](https://www.youtube.com/watch?v=CwdI-6pmFm4)

[![YouTube Video Link](https://img.youtube.com/vi/QEtHmv8D-tI/0.jpg)](https://www.youtube.com/watch?v=QEtHmv8D-tI)

### `en` 10.06.2021 - CH Open Business Lunch > [Why we built an open source Identity & Access Management](https://www.ch-open.ch/ch-open-business-events/ch-open-business-lunch/why-we-built-an-open-source-identity-access-management/)

[![YouTube Video Link](https://img.youtube.com/vi/Wly2jqbzP8Q/0.jpg)](https://www.youtube.com/watch?v=Wly2jqbzP8Q)

### `en` 25.03.2021 - CH Open Business Lunch > [Dev Environments as Code with Gitpod](https://www.ch-open.ch/ch-open-business-events/ch-open-business-lunch/dev-environments-as-code-with-gitpod/)

[![YouTube Video Link](https://img.youtube.com/vi/Z5vlYHBYlno/0.jpg)](https://www.youtube.com/watch?v=Z5vlYHBYlno)

### `de` 14.01.2021 - CH Open Business Lunch > [Moderne Datenarchitekturen mit Open Source](https://www.ch-open.ch/ch-open-business-events/ch-open-business-lunch/moderne-datenarchitektur-mit-open-source/)

[![YouTube Video Link](https://img.youtube.com/vi/f08TN2uwocs/0.jpg)](https://www.youtube.com/watch?v=f08TN2uwocs)

### `de` 26.11.2020 - CH Open Business Lunch > [Das Innovationspotential von OpenStreetMap und Wikidata / Wikipedia / Wikimedia Commons](https://www.ch-open.ch/ch-open-business-events/ch-open-business-lunch/das-innovationspotential-von-openstreetmap-und-wikidata-wikipedia-wikimedia-commons/)

[![YouTube Video Link](https://img.youtube.com/vi/hkRBAK8VFkI/0.jpg)](https://www.youtube.com/watch?v=hkRBAK8VFkI)

### `de` 14.08.2020 - CH Open Business Lunch > [Darf der Staat eigene Software unter eine Open-Source-Lizenz stellen? inkl. Open Source Software bei der SwissCovid-App](https://www.ch-open.ch/ch-open-business-events/ch-open-business-lunch/darf-der-staat-eigene-software-unter-eine-open-source-lizenz-stellen/)

[![YouTube Video Link](https://img.youtube.com/vi/7kEUn5g28Xw/0.jpg)](https://www.youtube.com/watch?v=7kEUn5g28Xw)

### `de` 20.02.2020 - CH Open Business Lunch > [The elephant in the Enterprise](https://www.ch-open.ch/ch-open-business-events/ch-open-business-lunch/archiv-obl-2020/)

[![YouTube Video Link](https://img.youtube.com/vi/598LJQvPFIA/0.jpg)](https://www.youtube.com/watch?v=598LJQvPFIA)
