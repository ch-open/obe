| guest        | host           |
| :----------: | :------------: | 
| ![](http://api.qrserver.com/v1/create-qr-code/?data=https%3A%2F%2Ftypefox.io&size=300x300&ecc=L) | ![](http://api.qrserver.com/v1/create-qr-code/?data=https%3A%2F%2Fgithub.com%2Fbaloise&size=300x300&ecc=L)| 

---

![](https://www.ch-open.ch/wp-content/uploads/2019/04/logo_chopen_web_big-1.png)

### [Open Business Event](https://gitlab.com/ch-open/obe/-/boards)

---

## 11 - 12 Uhr
Begrüssung, Referat "Dev Environments as Code with Gitpod" durch [Sven Efftinge, Co-Founder and CEO of Gitpod](https://twitter.com/svenefftinge)

Q&A: Sammlung im Chat

---

![](https://miro.medium.com/max/633/0*qpTpLLFLAKEmu4Ce)

---

## my 2 cents : ✅

gitpitch.com [>> gitpod.io + reveal.js](https://gitpod.io/#https://gitlab.com/ch-open/obe/-/tree/master/)

---

# what's next?

[CH Open Business MeetUp: JavaScript Web Development Frameworks](https://www.ch-open.ch/ch-open-business-meetup-javascript-web-development-frameworks/)

[PostgreSQL-Anwendertreffen](https://www.ch-open.ch/postgresql-anwendertreffen/)

---

# thank you

see you next time & stay safe!