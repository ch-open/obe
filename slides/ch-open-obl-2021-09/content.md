| guest        | host           |
| :----------: | :------------: | 
| ![](http://api.qrserver.com/v1/create-qr-code/?data=https%3A%2F%2Fjakarta.ee&size=300x300&ecc=L) | ![](http://api.qrserver.com/v1/create-qr-code/?data=https%3A%2F%2Fgithub.com%2Fbaloise&size=300x300&ecc=L)| 

---

[![](https://www.ch-open.ch/wp-content/uploads/2019/04/logo_chopen_web_big-1.png)](https://twitter.com/ch_open)

### [Open Business Event](https://gitlab.com/ch-open/obe/-/boards)

---

![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/YouTube_Logo_2017.svg/1000px-YouTube_Logo_2017.svg.png)

### [CH Open @ youtube.com](https://www.youtube.com/c/CHOpen/)

---

## 13 - 14

Get Involved in Jakarta EE with **[Tanja Obradovic](https://twitter.com/tanjaeclipse)**

Q&A: please use the chat &| unmute (afterwards?)

--

## break

we'll continue on the hour 

-- 

## 14 - 15

A Closer Look at Jakarta EE 10 with **[Ivar Grimstad](https://twitter.com/ivar_grimstad)**

Q&A: please use the chat &| unmute (afterwards?)

---

![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Jakarta_ee_logo_schooner_color_stacked_default.svg/632px-Jakarta_ee_logo_schooner_color_stacked_default.svg.png)

---

# what's next?

https://jakarta.ee/events/

https://dinacon.ch

---

# thank you

see you next time & stay safe!
