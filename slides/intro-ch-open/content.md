
[![](https://www.ch-open.ch/wp-content/uploads/2019/04/logo_chopen_web_big-1.png)](https://twitter.com/ch_open)

Verein zur Förderung von Open Source Software und offenen Standards in der Schweiz

since [1982](https://www.ch-open.ch/ueber-ch-open/vereinsgeschichte/) - [@MarkusTiede](https://twitter.com/MarkusTiede) - https://www.ch-open.ch

---

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Central_Bern_from_north.jpg/335px-Central_Bern_from_north.jpg)

200 Einzelmitglieder sowie rund 100 Kollektiv- und Premiummitglieder (Firmen und Behörden)

11 ehrenamtliche [Vorstand](https://www.ch-open.ch/ueber-ch-open/vorstand/)smitglieder

Aktivitäten in diversen [Working Groups](https://www.ch-open.ch/workinggroups/)

---

# Events

[B](https://youtu.be/BIMDSYk0RSU)ildung: https://openeducationday.ch

Hands-on: https://workshoptage.ch

[N](https://youtu.be/NvIV9keIzc8)achhaltigkeit: https://dinacon.ch

[IT-Beschaffungskonferenz](https://www.ch-open.ch/it-beschaffungskonferenz-ch-open/)

[Open Business Events](https://www.ch-open.ch/ch-open-business-events/) : morgens, mittags & abends

---

# Infrastruktur

Nutzer & Anbieter von OSS: https://www.ossdirectory.com

Bildung: https://www.openeduserver.ch

[BigBlueButton](https://www.ch-open.ch/bigbluebutton/)

---

## [Presse](https://www.ch-open.ch/ueber-ch-open/presse/)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/YouTube_Logo_2017.svg/500px-YouTube_Logo_2017.svg.png)](https://www.youtube.com/c/CHOpen/)

https://oss-studie.ch

---

### [be(come) a member!](https://www.ch-open.ch/ueber-ch-open/mitgliedschaft/#anmeldung)