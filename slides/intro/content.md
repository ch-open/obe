| guest        | host           |
| :----------: | :------------: | 
| ![](http://api.qrserver.com/v1/create-qr-code/?data=https%3A%2F%2Fexample.org&size=300x300&ecc=L) | ![](http://api.qrserver.com/v1/create-qr-code/?data=https%3A%2F%2Fgithub.com%2Fbaloise&size=300x300&ecc=L)| 

---

[![](https://www.ch-open.ch/wp-content/uploads/2019/04/logo_chopen_web_big-1.png)](https://twitter.com/ch_open)

### [Open Business Event](https://gitlab.com/ch-open/obe/-/boards)

---

![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/YouTube_Logo_2017.svg/1000px-YouTube_Logo_2017.svg.png)

### [CH Open @ youtube.com](https://www.youtube.com/c/CHOpen/)

---

## 11 - 12 Uhr
Begrüssung, Referat "Thema des Vortrags" durch [Referent](https://pleasePutHereAValid.link)

Q&A: Sammlung im Chat

---

![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Die_drei_fragezeichen.svg/1000px-Die_drei_fragezeichen.svg.png)

---

# what's next?

---

# thank you

see you next time & stay safe!